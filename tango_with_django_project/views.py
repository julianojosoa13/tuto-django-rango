from django.shortcuts import render
from django.http import HttpResponse

def home(request):
    return HttpResponse("""<a href="/rango/">Go to Rango</a>""")