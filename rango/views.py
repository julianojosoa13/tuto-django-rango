# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    context ={'boldmessage': "I am bold font from the context"}
    return render(request, 'rango/index.html', context)

def about(request):
    context = {'italicmessage' : "I am an Italic message from the context"}
    return render(request, 'rango/about.html', context)